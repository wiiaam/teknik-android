package com.wiiaam.teknik.activities

import android.app.{Activity, NotificationManager}
import android.content.{Intent, Context}
import android.net.Uri
import android.os.{StrictMode, Bundle}
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.wiiaam.teknik.api.TeknikUploader
import com.wiiaam.teknik.utils.{UploadManager, TeknikNotificationManager}


class ShareActivity extends Activity {

  protected override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build())
    val intent = getIntent
    val extras = intent.getExtras
    val action = intent.getAction

    if (action == Intent.ACTION_SEND) {
      if (extras.containsKey(Intent.EXTRA_STREAM)) {
        val uri = extras.getParcelable[Uri](Intent.EXTRA_STREAM)
        Log.d(this.getClass.getSimpleName, "Uploading file, " + uri.toString)
        UploadManager.uploadFile(getApplicationContext, uri)
      }
    }
  }
}
