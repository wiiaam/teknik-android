package com.wiiaam.teknik.data

object UploadType extends Enumeration {
  type UploadType = Value

  val FILE = Value("file")
  val PASTE = Value("paste")

}
