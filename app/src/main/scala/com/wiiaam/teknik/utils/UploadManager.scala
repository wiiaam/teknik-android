package com.wiiaam.teknik.utils

import android.app.PendingIntent
import android.content.{Intent, Context}
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.wiiaam.teknik.R
import com.wiiaam.teknik.api.TeknikUploader
import com.wiiaam.teknik.api.TeknikUploader.UploadProgress


object UploadManager {

  def uploadFile(ctx: Context, fileLocation: Uri): Unit = {
    val parsedUri = parseUriToFilename(ctx, fileLocation)
    val filename = parsedUri.split("/")(parsedUri.split("/").length-1)

    var title = ctx.getResources.getString(R.string.upload_desc)
    val notifid = TeknikNotificationManager.createProgressNotification(ctx, title, filename, None)

    val subscriber = TeknikUploader.upload(ctx, parsedUri)


    subscriber.foreach {
      case Left(progress) =>
        Log.d(UploadManager.getClass.getSimpleName, s"Updating progress $filename ${progress.bytesWritten} / ${progress.totalSize}")
        title = ctx.getResources.getString(R.string.upload_desc)
        TeknikNotificationManager.updateProgressNotification(ctx, notifid, progress.bytesWritten, progress.totalSize, title, filename, None)
      case Right(result) =>
        if(result.success){
          title = ctx.getResources.getString(R.string.upload_comp)
          val url = result.file.get.url
          Log.d(UploadManager.getClass.getSimpleName, s"Upload completed $filename to $url")
          val browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(result.file.get.url))
          val pendingIntent = PendingIntent.getActivity(ctx, 0, browserIntent, PendingIntent.FLAG_UPDATE_CURRENT)
          TeknikNotificationManager.completeProgressNotification(ctx, notifid, title, url, Some(pendingIntent), None)
        }
        else{
          title = ctx.getResources.getString(R.string.upload_fail)
          Log.d(UploadManager.getClass.getSimpleName,s"Upload failed $filename")
          TeknikNotificationManager.completeProgressNotification(ctx, notifid, title, filename, None, None)
        }

    }
  }

  private def parseUriToFilename(ctx: Context, uri: Uri): String = {
    var selectedImagePath: String = null
    val fileManagerPath: String = uri.getPath
    val projection: Array[String] = Array("_data")
    val cursor = ctx.getContentResolver.query(uri, projection, null, null, null)
    if (cursor != null) {
      val column_index = cursor.getColumnIndexOrThrow("_data")
      cursor.moveToFirst()
      selectedImagePath = cursor.getString(column_index)
    }
    if (selectedImagePath != null) {
      selectedImagePath
    }
    else if (fileManagerPath != null) {
      fileManagerPath
    }
    else null
  }
}
