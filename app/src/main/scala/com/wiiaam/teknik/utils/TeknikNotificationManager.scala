package com.wiiaam.teknik.utils

import android.app.{PendingIntent, Notification, NotificationManager}
import android.content.Context
import android.graphics.Bitmap
import android.support.v4.app.NotificationCompat
import com.wiiaam.teknik.R

import scala.util.Random


object TeknikNotificationManager {


  var mNotificationManager: Option[NotificationManager] = None

  def checkManagerDefined(ctx: Context): Unit ={
    if(mNotificationManager.isEmpty){
      mNotificationManager = Some(ctx.getSystemService(Context.NOTIFICATION_SERVICE).asInstanceOf[NotificationManager])
    }
  }

  def createProgressNotification(ctx: Context, title: String, text: String, bitmap: Option[Bitmap]): Int = {
    checkManagerDefined(ctx)
    val notificationID = Random.nextInt(Integer.MAX_VALUE)

    val mBuilder = new NotificationCompat.Builder(ctx)
      .setSmallIcon(R.drawable.icon)
      .setContentTitle(title)
      .setContentText(text)
      .setProgress(100, 0, true)

    bitmap.foreach(bm => mBuilder.setLargeIcon(bm))

    val notification = mBuilder.build()

    notification.flags = Notification.FLAG_ONGOING_EVENT

    mNotificationManager.foreach(_.notify(notificationID, notification))
    notificationID
  }

  def updateProgressNotification(ctx: Context, notificationID: Int, progress: Long, total: Long, title: String, text: String, bitmap: Option[Bitmap]): Unit = {
    checkManagerDefined(ctx)
    val prog: Int = math.floor((progress.asInstanceOf[Double] / total.asInstanceOf[Double]) * 100).asInstanceOf[Int]
    val percent = prog + "%"
    val mBuilder = new NotificationCompat.Builder(ctx)
      .setSmallIcon(R.drawable.icon)
      .setContentTitle(title)
      .setContentText(text)
      .setProgress(total.asInstanceOf[Int], progress.asInstanceOf[Int], false)
      .setContentInfo(percent)

    bitmap.foreach(bm => mBuilder.setLargeIcon(bm))

    val notification = mBuilder.build()
    notification.flags = Notification.FLAG_ONGOING_EVENT
    mNotificationManager.foreach(_.notify(notificationID, notification))
  }

  def completeProgressNotification(ctx: Context, notificationID: Int, title: String, text: String, intent: Option[PendingIntent], bitmap: Option[Bitmap]): Unit = {
    checkManagerDefined(ctx)
    val mBuilder = new NotificationCompat.Builder(ctx)
      .setSmallIcon(R.drawable.icon)
      .setContentTitle(title)
      .setContentText(text)
    bitmap.foreach(bm => mBuilder.setLargeIcon(bm))

    intent.foreach(intent => mBuilder.setContentIntent(intent))

    val notification = mBuilder.build()
    mNotificationManager.foreach(_.notify(notificationID, notification))

  }
}
