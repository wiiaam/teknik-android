package com.wiiaam.teknik.api


object ApiUrls {

  val UPLOAD = "https://api.teknik.io/upload/post"
  val PASTE = "https://api.teknik.io/paste"
}
