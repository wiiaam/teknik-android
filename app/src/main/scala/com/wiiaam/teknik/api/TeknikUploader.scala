package com.wiiaam.teknik.api

import android.content.Context
import android.net.Uri
import android.util.Log
import com.loopj.android.http._
import com.wiiaam.teknik.data.TeknikFile
import org.json.{JSONArray, JSONObject}
import rx.lang.scala.Observable
import java.io.File
import cz.msebera.android.httpclient.Header

import scala.Predef

object TeknikUploader {

  type Upload = Either[UploadProgress, UploadResult]
  case class UploadResult(success: Boolean, file: Option[TeknikFile])
  case class UploadProgress(bytesWritten: Long, totalSize: Long)

  def upload(ctx: Context, fileLocation: String): Observable[Upload] = {
    Observable(subscriber => {
      try{
        val uploadFile = new File(fileLocation)
        val params = new RequestParams()
        params.put("file", uploadFile)
        params.put("get_delete_key", "yes")
        val client = new SyncHttpClient(true, 80, 443)
        client.post(ctx, ApiUrls.UPLOAD, params, new JsonHttpResponseHandler {
          override def onFailure(statusCode: Int, headers: Array[Header], error: Throwable, responseBody: JSONObject): Unit = {
            subscriber.onNext(Right(UploadResult(success = false, None)))
          }

          override def onProgress(bytesWritten: Long, total: Long): Unit = {
            Log.d("progress", "onprogress")
            subscriber.onNext(Left(UploadProgress(bytesWritten, total)))
          }

          override def onSuccess(statusCode: Int, headers: Array[Header], response: JSONArray): Unit = {
            val json = response.getJSONObject(0).getJSONObject("results").getJSONObject("file")
            val teknikFile = TeknikFile(json.getString("name"), json.getString("url"),
              json.getString("type"), json.getInt("size"), json.getString("delete_key"))
            subscriber.onNext(Right(UploadResult(success = true, Some(teknikFile))))
          }
        })

        Log.d("progress", "finished")
      }
      catch {
        case e: Exception =>
          Log.e(TeknikUploader.getClass.getSimpleName, "Failed due to exception: " + fileLocation, e)
          subscriber.onNext(Right(UploadResult(success = false,None)))
      }

      subscriber.onCompleted()
    })
  }

}
