# Teknik for Android

Adds an option for Teknik uploads in the share menu

### Coming soon

- Upload history
- Checking for duplicate uploads
- DATABASE

## License
Copyright © 2015 William Howell

All works are licensed under the terms of [the GPL v3](LICENSE).